using System;

namespace Domain.Model
{
	public class ProductInfo : IEntity
	{
		private int _id;
		private String _description;
		private Decimal _purchasePrice;
		private Decimal _salePrice;
		private Product _product;

		public ProductInfo ()
		{
		}

		public virtual int Id {
			get { return _id; }
		}

		public virtual String Description {
			get { return _description;}
			set { _description = value; }
		}

		public virtual Decimal PurchasePrice {
			get { return _purchasePrice;}
			set { _purchasePrice = value; }
		}

		public virtual Decimal SalePrice {
			get { return _salePrice;}
			set { _salePrice = value; }
		}

		public virtual Product Product {
			get { return _product;}
			set { 
				_product = value;
				if (_product.ProductInfo != this)
					_product.ProductInfo = this;
			}
		}
	}
}

