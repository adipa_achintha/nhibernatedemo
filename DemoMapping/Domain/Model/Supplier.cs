using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Domain.Model
{
	public class Supplier : IEntity
	{
		private int _id;
		private String _code;
		private String _name;
		private IList<Product> _products;

		public Supplier ()
		{
			_products = new List<Product>();
		}

		public virtual int Id {
			get { return _id; }
		}

		public virtual String Code {
			get { return _code;}
			set { _code = value; }
		}

		public virtual String Name {
			get { return _name;}
			set { _name = value; }
		}

		public virtual void Add (Product product)
		{
			_products.Add(product);
			if (!product.Suppliers.Contains(this))
				product.Add(this);
		}

		public virtual ReadOnlyCollection<Product> Products {
			get {return new ReadOnlyCollection<Product>(_products);}
		}
	}
}

