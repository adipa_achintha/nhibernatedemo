using System;
using NUnit.Framework;
using DomainTest.Context;
using Domain.Model;

namespace DomainTest
{
	[TestFixture]
	public class ProductInfoContextTest
	{
		[Test]
		public void Create_WithDetails_ShouldSetValues()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.ProductInfo.WithDetails("description",1000m,440.5m).Create("pi1");
				ctx.FlushNClear();

				var productInfo = ctx.ProductInfo.Get("pi1");
				Assert.IsNotNull(productInfo);
				Assert.IsTrue(productInfo.Id > 0);
				Assert.AreEqual("description",productInfo.Description);
				Assert.AreEqual(1000m,productInfo.PurchasePrice);
				Assert.AreEqual(440.5m,productInfo.SalePrice);
			}
		}

		[Test]
		public void Create_WithProduct_ShouldSetProduct()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Product.Create("p1");
				ctx.ProductInfo.ForProduct("p1").Create("pi1");
				ctx.FlushNClear();

				var productInfo = ctx.ProductInfo.Get("pi1");
				var product = ctx.Product.Get("p1");
				Assert.IsNotNull(product);
				Assert.IsNotNull(productInfo);
				Assert.AreEqual(productInfo,product.ProductInfo);
				Assert.AreEqual(product,productInfo.Product);

			}
		}
	}
}

