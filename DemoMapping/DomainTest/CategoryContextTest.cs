using System;
using NUnit.Framework;
using DomainTest.Context;
using Domain.Model;

namespace DomainTest
{
	[TestFixture]
	public class CategoryContextTest
	{
		[Test]
		public void Create_WithNameAndCode_ShouldSetValues()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Category.WithCodeAndName("code","name").Create("c1");
				ctx.FlushNClear();

				var category = ctx.Category.Get("c1");
				Assert.IsNotNull(category);
				Assert.IsTrue(category.Id > 0);
				Assert.AreEqual("code",category.Code);
				Assert.AreEqual("name",category.Name);
			}
		}

		[Test]
		public void Create_WithChildren_ShouldSetChildren()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Category.Create("childA");
				ctx.Category.Create("childB");
				ctx.Category.ParentOf("childA").ParentOf("childB").Create("parent");
				ctx.FlushNClear();

				var childA = ctx.Category.Get("childA");
				var childB = ctx.Category.Get("childB");
				var parent = ctx.Category.Get("parent");
				Assert.AreEqual(parent,childA.Parent);
				Assert.AreEqual(parent,childB.Parent);
				Assert.IsTrue(parent.Children.Contains(childA));
				Assert.IsTrue(parent.Children.Contains(childB));
			}
		}

		[Test]
		public void Create_WithParent_ShouldSetChildren()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Category.Create("parent");
				ctx.Category.ChildOf("parent").Create("childA");
				ctx.Category.ChildOf("parent").Create("childB");

				ctx.FlushNClear();

				var childA = ctx.Category.Get("childA");
				var childB = ctx.Category.Get("childB");
				var parent = ctx.Category.Get("parent");
				Assert.AreEqual(parent,childA.Parent);
				Assert.AreEqual(parent,childB.Parent);
				Assert.IsTrue(parent.Children.Contains(childA));
				Assert.IsTrue(parent.Children.Contains(childB));
			}
		}
	}
}

