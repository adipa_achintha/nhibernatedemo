using System;

namespace DomainTest.Infrastructure
{
	public class EntityAlreadyExistsException<T> : Exception
	{
		public EntityAlreadyExistsException (string alias)
			: base(string.Format("cannot create entity new as it already exists for type {0} for alias {1}",typeof(T),alias))
		{
		}
	}
}

