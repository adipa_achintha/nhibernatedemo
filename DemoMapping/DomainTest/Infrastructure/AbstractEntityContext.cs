using System;
using Domain;
using System.Collections.Generic;
using DomainTest.Context;

namespace DomainTest.Infrastructure
{
	public abstract class AbstractEntityContext<T> where T:IEntity
	{
		private DomainTestContext _testContext;
		private Dictionary<string,int> _entities;
		private T _current;

		public AbstractEntityContext (DomainTestContext testContext)
		{
			_testContext = testContext;
			_entities = new Dictionary<string, int>();
		}

		protected T Current {
			get {
				if (_current == null)
					_current = Create ();
				return _current;
			}
		}

		protected DomainTestContext TestContext {
			get {
				return _testContext;
			}
		}

		public T Get (string alias)
		{
			if (!_entities.ContainsKey(alias))
				throw new EntityNotFoundException<T>(alias);

			int key = _entities[alias];
			T value = _testContext.Session.Get<T>(key);
			return value;
		}

		public T Create (string alias)
		{
			if (_entities.ContainsKey(alias)) 
				throw new EntityAlreadyExistsException<T>(alias);

			T copy = _current;
			if (copy == null)
				copy = Create();

			_testContext.Session.Save(copy);
			_entities.Add(alias,copy.Id);
			_current = default(T);
			return copy;
		}

		protected abstract T Create();
	}
}

