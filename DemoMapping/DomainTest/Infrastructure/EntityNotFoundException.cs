using System;

namespace DomainTest.Infrastructure
{
	public class EntityNotFoundException<T> : Exception
	{
		public EntityNotFoundException(string alias) 
			: base(string.Format("could not found entity type {0} for alias {1}",typeof(T),alias))
		{
		}
	}
}

