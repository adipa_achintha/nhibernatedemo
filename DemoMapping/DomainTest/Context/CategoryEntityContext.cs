using System;
using Domain.Model;
using DomainTest.Infrastructure;

namespace DomainTest.Context
{
	public class CategoryEntityContext : AbstractEntityContext<Category>
	{
		public CategoryEntityContext (DomainTestContext testContext) : base(testContext)
		{
		}

		public CategoryEntityContext WithCodeAndName (String code, String name)
		{
			Current.Code = code;
			Current.Name = name;
			return this;
		}

		public CategoryEntityContext ChildOf (String alias)
		{
			var parent = Get(alias);
			Current.Parent = parent;
			return this;
		}

		public CategoryEntityContext ParentOf (String alias)
		{
			var child = Get(alias);
			Current.Add(child);
			return this;
		}

		protected override Category Create ()
		{
			var product = new Category();
			product.Code = Guid.NewGuid().ToString().Substring(0,5);
			product.Name = Guid.NewGuid().ToString().Substring(0,10);
			return product;
		}
	}
}

