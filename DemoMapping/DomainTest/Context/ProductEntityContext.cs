using System;
using Domain.Model;
using DomainTest.Infrastructure;

namespace DomainTest.Context
{
	public class ProductEntityContext : AbstractEntityContext<Product>
	{
		public ProductEntityContext (DomainTestContext testContext) : base(testContext)
		{
		}

		public ProductEntityContext WithCodeAndName (String code, String name)
		{
			Current.Code = code;
			Current.Name = name;
			return this;
		}

		public ProductEntityContext WithDetails (String alias)
		{
			var productInfo = TestContext.ProductInfo.Get(alias);
			Current.ProductInfo = productInfo;
			return this;
		}

		public ProductEntityContext SuppliedBy (String alias)
		{
			var supplier = TestContext.Supplier.Get(alias);
			Current.Add(supplier);
			return this;
		}

		public ProductEntityContext OfCategory (String alias)
		{
			var category = TestContext.Category.Get(alias);
			Current.Category = category;
			return this;
		}

		protected override Product Create ()
		{
			var product = new Product();
			product.Code = Guid.NewGuid().ToString().Substring(0,5);
			product.Name = Guid.NewGuid().ToString().Substring(0,10);
			return product;
		}
	}
}

