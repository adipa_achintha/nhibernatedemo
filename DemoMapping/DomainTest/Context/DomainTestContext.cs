using System;
using NHibernate;
using NHibernate.Cfg;
using Domain.Model;
using Domain;
using NUnit.Framework;
using System.Collections.Generic;
using NHibernate.Tool.hbm2ddl;

namespace DomainTest.Context
{
	public class DomainTestContext : IDisposable
	{
		private static ISessionFactory Factory;

		private ISession _session;
		private ITransaction _transaction;

		public DomainTestContext ()
		{
			if (Factory == null) {

				var cfg = new Configuration ();
				cfg.Configure ();
				cfg.AddAssembly (typeof(Product).Assembly);

				new SchemaExport(cfg).Execute(false, true, false);

				// Get ourselves an NHibernate Session
				Factory = cfg.BuildSessionFactory ();
			}

			_session = Factory.OpenSession();
			_transaction = _session.BeginTransaction();
		}

		protected internal ISession Session {
			get { return _session; }
		}

		private ProductEntityContext _product;
		public ProductEntityContext Product {
			get {
				if (_product == null) 
					_product = new ProductEntityContext (this);
				return _product;
			}
		}

		private ProductInfoEntityContext _productInfo;
		public ProductInfoEntityContext ProductInfo {
			get {
				if (_productInfo == null) 
					_productInfo = new ProductInfoEntityContext (this);
				return _productInfo;
			}
		}


		private SupplierEntityContext _supplier;
		public SupplierEntityContext Supplier {
			get {
				if (_supplier == null) 
					_supplier = new SupplierEntityContext (this);
				return _supplier;
			}
		}

		private CategoryEntityContext _category;
		public CategoryEntityContext Category {
			get {
				if (_category == null) 
					_category = new CategoryEntityContext (this);
				return _category;
			}
		}

		public void FlushNClear ()
		{
			_session.Flush();
			_session.Clear();
		}

		public void Dispose ()
		{
			_transaction.Rollback();
			_session.Clear();
			_session.Close();
		}
	}
}

