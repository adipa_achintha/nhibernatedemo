using System;
using Domain.Model;
using DomainTest.Infrastructure;

namespace DomainTest.Context
{
	public class ProductInfoEntityContext : AbstractEntityContext<ProductInfo>
	{
		public ProductInfoEntityContext (DomainTestContext testContext) : base(testContext)
		{
		}

		public ProductInfoEntityContext WithDetails (String description, Decimal purchasePrice, Decimal salePrice)
		{
			Current.Description = description;
			Current.PurchasePrice = purchasePrice;
			Current.SalePrice = salePrice;
			return this;
		}

		public ProductInfoEntityContext ForProduct (String alias)
		{
			var product = TestContext.Product.Get(alias);
			Current.Product = product;
			return this;
		}

		protected override ProductInfo Create ()
		{
			var productInfo = new ProductInfo();
			productInfo.Description = Guid.NewGuid().ToString().Substring(0,5);
			productInfo.PurchasePrice = 0m;
			productInfo.SalePrice = 0m;
			return productInfo;
		}
	}
}

