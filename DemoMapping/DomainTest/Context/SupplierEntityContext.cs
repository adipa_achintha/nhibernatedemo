using System;
using Domain.Model;
using DomainTest.Infrastructure;

namespace DomainTest.Context
{
	public class SupplierEntityContext : AbstractEntityContext<Supplier>
	{
		public SupplierEntityContext (DomainTestContext testContext) : base(testContext)
		{
		}

		public SupplierEntityContext WithCodeAndName (String code, String name)
		{
			Current.Code = code;
			Current.Name = name;
			return this;
		}

		public SupplierEntityContext SuppliesProduct (String alias)
		{
			var product = TestContext.Product.Get(alias);
			Current.Add(product);
			return this;
		}

		protected override Supplier Create ()
		{
			var supplier = new Supplier();
			supplier.Code = Guid.NewGuid().ToString().Substring(0,5);
			supplier.Name = Guid.NewGuid().ToString().Substring(0,10);
			return supplier;
		}
	}
}

