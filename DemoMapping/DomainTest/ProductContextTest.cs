using System;
using NUnit.Framework;
using DomainTest.Context;
using Domain.Model;
using DomainTest.Infrastructure;

namespace DomainTest
{
	[TestFixture]
	public class ProductContextTest
	{
		[Test]
		[ExpectedException(typeof(EntityAlreadyExistsException<Product>))]
		public void Create_ExistingAlias_ShouldThrowException()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Product.Create("p1");
				ctx.Product.Create("p1");
			}
		}

		[Test]
		[ExpectedException(typeof(EntityNotFoundException<Product>))]
		public void Get_NonExistingAlias_ShouldThrowException()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Product.Get("p1");
			}
		}

		[Test]
		public void Create_WithNameAndCode_ShouldSetValues()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Product.WithCodeAndName("code","name").Create("p1");
				ctx.FlushNClear();

				var product = ctx.Product.Get("p1");
				Assert.IsNotNull(product);
				Assert.IsTrue(product.Id > 0);
				Assert.AreEqual("code",product.Code);
				Assert.AreEqual("name",product.Name);
			}
		}

		[Test]
		public void Create_WithDetails_ShouldSetValues()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.ProductInfo.WithDetails("description",200.34m,300m).Create("pi1");
				ctx.Product.WithDetails("pi1").Create("p1");
				ctx.FlushNClear();

				var product = ctx.Product.Get("p1");
				Assert.IsNotNull(product);
				Assert.IsNotNull(product.ProductInfo);
				Assert.AreEqual("description",product.ProductInfo.Description);
				Assert.AreEqual(200.34,product.ProductInfo.PurchasePrice);
				Assert.AreEqual(300,product.ProductInfo.SalePrice);
			}
		}

		[Test]
		public void Create_WithSuppliers_ShouldSetSuppliers()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Supplier.Create("s1");
				ctx.Supplier.Create("s2");
				ctx.Product.SuppliedBy("s1").SuppliedBy("s2").Create("p1");
				ctx.FlushNClear();

				var supplierA = ctx.Supplier.Get("s1");
				var supplierB = ctx.Supplier.Get("s2");
				var product = ctx.Product.Get("p1");
				Assert.IsNotNull(product);
				Assert.IsTrue(product.Id > 0);
				Assert.IsTrue(product.Suppliers.Contains(supplierA));
				Assert.IsTrue(product.Suppliers.Contains(supplierB));
			}
		}

		[Test]
		public void Create_WithCategory_ShouldSetCategory()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Category.Create("c1");
				ctx.Product.OfCategory("c1").Create("p1");
				ctx.FlushNClear();

				var category = ctx.Category.Get("c1");
				var product = ctx.Product.Get("p1");
				Assert.IsNotNull(product);
				Assert.IsTrue(product.Id > 0);
				Assert.AreEqual(category,product.Category);
			}
		}
	}
}

