using System;
using NUnit.Framework;
using DomainTest.Context;
using Domain.Model;

namespace DomainTest
{
	[TestFixture]
	public class SupplierContextTest
	{
		[Test]
		public void Create_WithNameAndCode_ShouldSetValues()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Supplier.WithCodeAndName("code","name").Create("s1");
				ctx.FlushNClear();

				var supplier = ctx.Supplier.Get("s1");
				Assert.IsNotNull(supplier);
				Assert.IsTrue(supplier.Id > 0);
				Assert.AreEqual("code",supplier.Code);
				Assert.AreEqual("name",supplier.Name);
			}
		}

		[Test]
		public void Create_WithProducts_ShouldSetProducts()
		{
			using(var ctx = new DomainTestContext())
			{
				ctx.Product.Create("p1");
				ctx.Product.Create("p2");
				ctx.Supplier.SuppliesProduct("p1").SuppliesProduct("p2").Create("s1");
				ctx.FlushNClear();

				var productA = ctx.Product.Get("p1");
				var productB = ctx.Product.Get("p2");
				var supplier = ctx.Supplier.Get("s1");
				Assert.IsNotNull(supplier);
				Assert.IsTrue(supplier.Id > 0);
				Assert.IsTrue(supplier.Products.Contains(productA));
				Assert.IsTrue(supplier.Products.Contains(productB));
			}
		}
	}
}

