using System;
using NUnit.Framework;
using Domain.Model;
using DomainTest.Infrastructure;
using FluentAssertions;

namespace DomainTest
{
	[TestFixture]
	public class CategoryPersistanceTest : TestBase
	{
		[Test]
		public void Create_WithNameAndCode_ShouldSetValues()
		{
			Category cat = new Category();
			cat.Code = "category code";
			cat.Name = "category name";

			Session.Save(cat);
			FlushNClear();

			Category retrieved = Session.Get<Category>(cat.Id);
			retrieved.Should().NotBeNull();
			retrieved.ShouldHave().AllPropertiesBut(r => r.Children).EqualTo(cat);
		}

		[Test]
		public void Create_WithChildren_ShouldSetChildren ()
		{
			Category childA = new Category(){Code="childA",Name="name childA"};
			Session.Save(childA);

			Category childB = new Category(){Code="childB",Name="name childB"};
			Session.Save(childB);

			Category parent = new Category(){Code="Parent",Name="name Parent"};
			parent.Add(childA);
			parent.Add(childB);
			Session.Save(parent);

			FlushNClear();

			Category retchildA = Session.Get<Category>(childA.Id);
			Category retchildB = Session.Get<Category>(childB.Id);
			Category retParent = Session.Get<Category>(parent.Id);
			Assert.AreEqual(retParent,retchildA.Parent);
			Assert.AreEqual(retParent,retchildB.Parent);
			Assert.IsTrue(retParent.Children.Contains(retchildA));
			Assert.IsTrue(retParent.Children.Contains(retchildB));
		}
	}
}

