using System;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Cfg;
using Domain.Model;
using NUnit.Framework;
using FluentNHibernate.Mapping;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Conventions.Inspections;

namespace DomainTest.Infrastructure
{
	public class TestBase
	{
		private static ISessionFactory Factory;

		private ISession _session;
		private ITransaction _transaction;

		public TestBase ()
		{
		}

		private static void BuildSchema(Configuration config)
		{
			new SchemaExport(config).Execute(false, true,false);
		}

		protected ISession Session {
			get {
				return _session;
			}
		}

		protected void FlushNClear ()
		{
			_session.Flush();
			_session.Clear();
		}

		[SetUp]
		protected void Setup ()
		{
			if (Factory == null) {
				const string driverType = "DomainTest.Driver.MonoSqliteDriver, DomainTest";

				Factory =  Fluently.Configure()
					.Database(SQLiteConfiguration.Standard
					          .Driver(driverType)
					          .ConnectionString("Data Source=nhibernate.db;Version=3")
					          .ShowSql())
						.Mappings(m => m.FluentMappings
						          .Conventions.Add(DefaultAccess.CamelCaseField(CamelCasePrefix.Underscore))
						          .AddFromAssemblyOf<Product>())
					.ExposeConfiguration(BuildSchema)
					.BuildSessionFactory();
				// Get ourselves an NHibernate Session
			}

			_session = Factory.OpenSession ();
			_transaction = _session.BeginTransaction ();
		}

		[TearDown]
		public void TearDown ()
		{
			_transaction.Rollback();
			_session.Clear();
			_session.Close();
		}
	}
}

