using System;
using NUnit.Framework;
using Domain.Model;
using DomainTest.Infrastructure;
using FluentAssertions;

namespace DomainTest
{
	[TestFixture]
	public class SupplierPersistanceTest : TestBase
	{
		[Test]
		public void Create_WithNameAndCode_ShouldSetValues()
		{
			Supplier supp = new Supplier();
			supp.Code = "supplier code";
			supp.Name = "supplier name";

			Session.Save(supp);
			FlushNClear();

			Supplier retrieved = Session.Get<Supplier>(supp.Id);
			retrieved.Should().NotBeNull();
			retrieved.Id.Should().BeGreaterThan(0);
			retrieved.ShouldHave().AllPropertiesBut(r => r.Products).EqualTo(supp);
		}

		[Test]
		public void Create_WithProducts_ShouldSetProducts()
		{
			Product prodA = new Product(){Code = "codeA",Name = "nameA"};
			Session.Save(prodA);

			Product prodB = new Product(){Code = "codeB",Name = "nameB"};
			Session.Save(prodB);

			Supplier supp = new Supplier(){Code = "supplier",Name="name supplier"};
			supp.Add(prodA);
			supp.Add(prodB);
			Session.Save(supp);
			FlushNClear();

			Product retProdA = Session.Get<Product>(prodA.Id);
			Product retProdB = Session.Get<Product>(prodB.Id);
			Supplier retrieved = Session.Get<Supplier>(supp.Id);
			retrieved.Should().NotBeNull();
			Assert.IsTrue(retrieved.Products.Contains(retProdA));
			Assert.IsTrue(retrieved.Products.Contains(retProdB));	
		}
	}
}

