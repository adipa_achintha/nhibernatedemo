using System;
using NUnit.Framework;
using Domain.Model;
using DomainTest.Infrastructure;
using FluentAssertions;

namespace DomainTest
{
	[TestFixture]
	public class ProductInfoPersistanceTest : TestBase
	{
		[Test]
		public void Create_WithDetails_ShouldSetValues()
		{
			ProductInfo info = new ProductInfo(){Description = "description"
				,PurchasePrice=440.5m,SalePrice=1000m};
			Session.Save(info);
			FlushNClear();

			ProductInfo retrieved = Session.Get<ProductInfo>(info.Id);
			retrieved.Should().NotBeNull();
			retrieved.Id.Should().BeGreaterThan(0);
			retrieved.ShouldHave().AllProperties().EqualTo(info);
		}

		[Test]
		public void Create_WithProduct_ShouldSetProduct()
		{
			Product prod = new Product(){Code = "code",Name = "name"};
			Session.Save(prod);

			ProductInfo info = new ProductInfo(){Description = "description"
				,PurchasePrice=440.5m,SalePrice=1000m,Product = prod};
			Session.Save(info);
			FlushNClear();

			ProductInfo retrieved = Session.Get<ProductInfo>(info.Id);
			retrieved.Product.Should().NotBeNull();
			retrieved.Product.ShouldHave().AllPropertiesBut(p => p.ProductInfo).EqualTo(prod);
		}
	}
}

