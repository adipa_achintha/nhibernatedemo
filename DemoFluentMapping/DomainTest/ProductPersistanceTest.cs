using System;
using NUnit.Framework;
using Domain.Model;
using DomainTest.Infrastructure;
using FluentAssertions;

namespace DomainTest
{
	[TestFixture]
	public class ProductPersistanceTest : TestBase
	{
		[Test]
		public void Create_WithNameAndCode_ShouldSetValues()
		{
			Product prod = new Product();
			prod.Code = "product code";
			prod.Name = "product name";

			Session.Save(prod);
			FlushNClear();

			Product retrieved = Session.Get<Product>(prod.Id);
			retrieved.Should().NotBeNull();
			retrieved.Id.Should().BeGreaterThan(0);
			retrieved.ShouldHave().AllPropertiesBut(r => r.Suppliers).EqualTo(prod);
		}

		[Test]
		public void Create_WithDetails_ShouldSetValues()
		{
			ProductInfo info = new ProductInfo(){Description = "description"
				,PurchasePrice=200.34m,SalePrice=300m};
			Session.Save(info);

			Product prod = new Product(){Code = "code",Name = "name",ProductInfo = info};
			Session.Save(prod);
			FlushNClear();

			Product retrieved = Session.Get<Product>(prod.Id);
			retrieved.ProductInfo.Should().NotBeNull();
			retrieved.ProductInfo.ShouldHave().AllPropertiesBut(r => r.Product).EqualTo(info);
		}

		[Test]
		public void Create_WithSuppliers_ShouldSetSuppliers()
		{
			Supplier suppA = new Supplier(){Code = "suppA",Name="name suppA"};
			Session.Save(suppA);

			Supplier suppB = new Supplier(){Code = "suppB",Name="name suppB"};
			Session.Save(suppB);

			Product prod = new Product(){Code = "code",Name = "name"};
			prod.Add(suppA);
			prod.Add(suppB);
			Session.Save(prod);
			FlushNClear();


			Supplier retSuppA = Session.Get<Supplier>(suppA.Id);
			Supplier retSuppB = Session.Get<Supplier>(suppB.Id);
			Product retrieved = Session.Get<Product>(prod.Id);
			retrieved.Should().NotBeNull();
			Assert.IsTrue(retrieved.Suppliers.Contains(retSuppA));
			Assert.IsTrue(retrieved.Suppliers.Contains(retSuppB));
		}

		[Test]
		public void Create_WithCategory_ShouldSetCategory()
		{
			Category cat = new Category(){Code="cat",Name="name cat"};
			Session.Save(cat);

			Product prod = new Product(){Code = "code",Name = "name",Category = cat};
			Session.Save(prod);
			FlushNClear();

			Product retrieved = Session.Get<Product>(prod.Id);
			retrieved.Category.Should().NotBeNull();
			retrieved.Category.ShouldHave().AllPropertiesBut(r => r.Children).EqualTo(cat);
		}
	}
}

