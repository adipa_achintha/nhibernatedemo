using System;
using FluentNHibernate.Mapping;
using Domain.Model;
using FluentNHibernate.Conventions.Helpers;

namespace Domain.Mappings
{
	public class CategoryMap : ClassMap<Category>
	{
		public CategoryMap ()
		{
			Id(x => x.Id).UnsavedValue(-1).GeneratedBy.Native();
			Map(x => x.Code);
			Map(x => x.Name);

			References<Category>(x => x.Parent)
				.Access.CamelCaseField(Prefix.Underscore)
				.Column("parent_id")
				.NotFound.Ignore();

		    HasMany<Category>(x => x.Children)
				.AsBag()
				.Access.CamelCaseField(Prefix.Underscore)
				.Cascade.AllDeleteOrphan().Inverse()
				.KeyColumn("parent_id");
		}
	}
}

