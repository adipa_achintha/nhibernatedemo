using System;
using FluentNHibernate.Mapping;
using Domain.Model;

namespace Domain.Mappings
{
	public class SupplierMap : ClassMap<Supplier>
	{
		public SupplierMap ()
		{
			Id(x => x.Id).UnsavedValue(-1).GeneratedBy.Native();
			Map(x => x.Code);
			Map(x => x.Name);
			HasManyToMany<Product>(x => x.Products)
				.AsBag()
				.Access.CamelCaseField(Prefix.Underscore)
				.Table("product_x_supplier")
				.ParentKeyColumn("supplier_id")
				.ChildKeyColumn("product_id");
		}
	}
}

