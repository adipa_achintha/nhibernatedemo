using System;
using FluentNHibernate.Mapping;
using Domain.Model;

namespace Domain.Mappings
{
	public class ProductInfoMap : ClassMap<ProductInfo>
	{
		public ProductInfoMap ()
		{
			Id(x => x.Id).UnsavedValue(-1).GeneratedBy.Native();
			Map(x => x.Description);
			Map(x => x.PurchasePrice);
			Map(x => x.SalePrice);
			References<Product>(x => x.Product)
				.Access.CamelCaseField(Prefix.Underscore)
				.Column("product_id");
		}
	}
}

