using System;
using FluentNHibernate.Mapping;
using Domain.Model;

namespace Domain.Mappings
{
	public class ProductMap : ClassMap<Product>
	{
		public ProductMap ()
		{
			Id(x => x.Id).UnsavedValue(-1).GeneratedBy.Native();
			Map(x => x.Code);
			Map(x => x.Name);

			HasOne<ProductInfo>(x => x.ProductInfo)
				.Access.CamelCaseField(Prefix.Underscore);

			References<Category>(x => x.Category)
				//.Column("category_id")
				.Access.CamelCaseField(Prefix.Underscore);

			HasManyToMany<Supplier>(x => x.Suppliers)
				.AsBag()
				.Access.CamelCaseField(Prefix.Underscore)
				.Table("product_x_supplier").Inverse()
				.ParentKeyColumn("product_id")
				.ChildKeyColumn("supplier_id");
		}
	}
}

