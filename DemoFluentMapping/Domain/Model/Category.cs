using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Domain.Model
{
	public class Category 
	{
		private int _id;
		private String _code;
		private String _name;
		private Category _parent;
		private IList<Category> _children;

		public Category ()
		{
			_children = new List<Category>();
		}

		public virtual int Id {
			get { return _id; }
		}

		public virtual String Code {
			get { return _code;}
			set { _code = value; }
		}

		public virtual String Name {
			get { return _name;}
			set { _name = value; }
		}

		public virtual Category Parent {
			get { return _parent;}
			set { 
				_parent = value;
				if (_parent._children.Contains(this))
					_parent.Add(this);
			}
		}

		public virtual void Add (Category child)
		{
			if (!_children.Contains (child)) {
				_children.Add (child);
				child.Parent = this;
			}
		}

		public virtual ReadOnlyCollection<Category> Children {
			get {return new ReadOnlyCollection<Category>(_children);}
		}
	}
}

