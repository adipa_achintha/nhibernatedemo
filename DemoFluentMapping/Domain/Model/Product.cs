using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Domain.Model
{
	public class Product
	{
		private int _id;
		private String _code;
		private String _name;
		private ProductInfo _productInfo;
		private Category _category;
		private IList<Supplier> _suppliers;

		public Product ()
		{
			_suppliers = new List<Supplier>();
		}

		public virtual int Id {
			get { return _id; }
		}

		public virtual String Code {
			get { return _code;}
			set { _code = value; }
		}

		public virtual String Name {
			get { return _name;}
			set { _name = value; }
		}

		public virtual ProductInfo ProductInfo {
			get { return _productInfo;}
			set { 
				_productInfo = value; 
				if (_productInfo.Product != this)
					_productInfo.Product = this;
			}
		}

		public virtual Category Category {
			get { return _category;}
			set { _category = value; }
		}

		public virtual void Add (Supplier supplier)
		{
			_suppliers.Add(supplier);
			if (!supplier.Products.Contains(this))
				supplier.Add(this);
		}

		public virtual ReadOnlyCollection<Supplier> Suppliers {
			get {return new ReadOnlyCollection<Supplier>(_suppliers);}
		}
	}
}

